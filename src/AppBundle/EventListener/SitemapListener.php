<?php

namespace AppBundle\EventListener;

use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Doctrine\ORM\EntityManager;

use Presta\SitemapBundle\Service\SitemapListenerInterface;
use Presta\SitemapBundle\Event\SitemapPopulateEvent;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;

class SitemapListener implements SitemapListenerInterface
{
    private $router;
    private $entityManager;
    private $event;

    public function __construct(RouterInterface $router, EntityManager $entityManager)
    {
        $this->router = $router;
        $this->entityManager = $entityManager;
    }

    public function populateSitemap(SitemapPopulateEvent $event)
    {

        $this->event = $event;

        $section = $this->event->getSection();
        if (is_null($section) || $section == 'default') {
            //get absolute homepage url
            $url = $this->router->generate('homepage', array(), UrlGeneratorInterface::ABSOLUTE_URL);
            $this->sitemapAddUrl($url);

            $couriers = $this->entityManager->getRepository('AppBundle:User')->findAll();

            foreach($couriers as $courier) {
                $url = $this->router->generate('courier_show', ['slug' => $courier->getSlug()], true);
                $this->sitemapAddUrl($url);
            }

        }
    }

    private function sitemapAddUrl($url)
    {
        $this->event->getGenerator()->addUrl(
            new UrlConcrete(
                $url,
                new \DateTime(),
                UrlConcrete::CHANGEFREQ_DAILY,
                1.0
            ),
            'default'
        );
    }

}