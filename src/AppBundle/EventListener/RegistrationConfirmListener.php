<?php

namespace AppBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use FOS\UserBundle\Event\GetResponseUserEvent;
use AppBundle\Entity\Membership;
use AppBundle\Entity\Collection;

/**
 * Listener responsible to change the redirection at the end of the password resetting
 */
class RegistrationConfirmListener implements EventSubscriberInterface
{

    private $container;

    public function __construct($entityManager, ContainerInterface $container)
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_CONFIRM => 'onRegistrationConfirm',
        );
    }

    public function onRegistrationConfirm(GetResponseUserEvent $event)
    {

        // Add free Membership for 3 months
        $currentDate = new \DateTime('now');
        $dateTo = $currentDate->modify('+6 month');

        $membership = new Membership();
        $membership->setDateFrom(new \DateTime('now'));
        $membership->setDateTo($dateTo);
        $membership->setUser($event->getUser());
        $this->entityManager->persist($membership);
        $this->entityManager->flush();

        // Create collection records
        $countries = $this->entityManager->getRepository('AppBundle:Country')
            ->findAll();

        foreach ($countries as $country) {

            $collection = new Collection();
            $collection->setCountry($country);

            $user = $event->getUser();
            $user->addCollection($collection);
            $this->entityManager->persist($user);
            $this->entityManager->flush();

        }

        $logger = $this->container->get('logger');
        $logger->notice('Nowy uzytkownik zostal zarejestrowany');

    }
}