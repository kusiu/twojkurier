<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CollectionFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
            'collections',
            'bootstrap_collection',
                array(
                    'label' => 'form.collection',
                    'translation_domain' => 'FOSUserBundle',
                    'type'  => new CollectionType(),
//                    'allow_add'          => true,
//                    'allow_delete'       => true,
//                    'add_button_text'    => 'Dodaj',
//                    'delete_button_text' => 'Usuń',
                    'sub_widget_col'     => 10,
                    'button_col'         => 3

                )
            )
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_collectionform';
    }
}
