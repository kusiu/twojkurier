<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class OfferType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('countryFrom', 'entity', array(
                'class'    => 'AppBundle:Country',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.name', 'ASC');
                },
                'label'    => 'z',
                'attr'  => array (
                    'class' => 'form-control country-from',
                    'readonly' => true
                )
            ))
            ->add('countryTo', 'entity', array(
                'class'    => 'AppBundle:Country',
                'label' => 'do',
                'attr'  => array (
                    'class' => 'form-control country-to',
                    'readonly' => true
                )
            ))
            ->add('weightFrom', null,
                array(
                    'label' => 'od',
                    'attr' => array(
                        'placeholder' => 'np. 1',
                    ),
                    ))
            ->add('weightTo', null,
                array(
                    'label' => 'do',
                    'attr' => array(
                        'placeholder' => 'np. 10',
                    ),
                ))
            ->add('price', null,
                array(
                    'label' => 'cena',
                    'attr' => array(
                        'placeholder' => 'np. 30',
                    )
                ))
            ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Offer'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_offer';
    }
}
