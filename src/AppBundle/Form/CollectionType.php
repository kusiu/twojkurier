<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CollectionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('country', 'entity', array(
                'class'    => 'AppBundle:Country',
                'label' => 'Z Kraju',
                'disabled' => true
            ))
            ->add('days', 'entity', array(
                'class'    => 'AppBundle:Day',
                'label' => 'Dni odbioru',
                'expanded' => true,
                'multiple' => true,
                'attr'  => array (
                    'class' => 'form-control days'
                )
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Collection'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_collection';
    }
}
