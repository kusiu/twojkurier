<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        // add your custom field
        $builder->remove('email')
            ->remove('plainPassword')
            ->remove('username');

        $builder->add('firstName', null, ['label' => 'form.first_name', 'translation_domain' => 'FOSUserBundle'])
        ->add('lastName', null, ['label' => 'form.last_name', 'translation_domain' => 'FOSUserBundle'])
        ->add('company', null, ['label' => 'form.company', 'translation_domain' => 'FOSUserBundle'])
        ->add('email', 'email', array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle'))
        ->add('plainPassword', 'repeated', array(
            'type' => 'password',
            'options' => array('translation_domain' => 'FOSUserBundle'),
            'first_options' => array('label' => 'form.password'),
            'second_options' => array('label' => 'form.password_confirmation'),
            'invalid_message' => 'fos_user.password.mismatch',
        ))
        ->add('services', 'entity', array(
            'class'    => 'AppBundle:Service',
            'label'    => ['label' => 'form.services', 'translation_domain' => 'FOSUserBundle'],
        ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'app_user_registration';
    }
}