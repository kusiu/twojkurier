<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use libphonenumber\PhoneNumberFormat;

class ProfileFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        // add your custom field
        $builder->remove('email')
            ->remove('plainPassword')
            ->remove('username');

        $builder->add('firstName', null, ['label' => 'form.first_name', 'translation_domain' => 'FOSUserBundle', 'label_attr' => ['class' => 'required']])
        ->add('lastName', null, ['label' => 'form.last_name', 'translation_domain' => 'FOSUserBundle'])
        ->add('company', null, ['label' => 'form.company', 'translation_domain' => 'FOSUserBundle'])
        ->add('britishPhoneNumber', null, ['label' => 'form.britishPhoneNumber', 'translation_domain' => 'FOSUserBundle', 'label_attr' => ['class' => 'required']])
        ->add('polishPhoneNumber', null, ['label' => 'form.polishPhoneNumber', 'translation_domain' => 'FOSUserBundle', 'label_attr' => ['class' => 'required']])
        ->add('contactEmail', null, ['label' => 'form.contactEmail', 'translation_domain' => 'FOSUserBundle', 'label_attr' => ['class' => 'required']])
        ->add('website', null, ['label' => 'form.website', 'translation_domain' => 'FOSUserBundle'])
        ->add('address', null, ['label' => 'form.address', 'translation_domain' => 'FOSUserBundle'])
        ->add('logo', 'vich_image', array(
            'required'      => false,
            'download_link' => true,
            'label'          => 'form.logo',
            'translation_domain' => 'FOSUserBundle'
        ))

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
            'property_name'     => 'path'
        ));
    }

    public function getName()
    {
        return 'app_user_profile';
    }
}