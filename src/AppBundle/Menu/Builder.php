<?php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options) {
        
        // hopefully temporary solution :)
        $request = $this->container->get('request');
        
        $id = 1;
        if ($request->get('id')) {
            $id = $request->get('id');
        }

        $menu = $factory->createItem('root');

        $menu->addChild('home', array('route' => 'homepage', 'label' => 'menu.home'));
        $menu['home']->addChild('home', array('route' => 'homepage', 'label' => 'menu.home'));
        $menu['home']->addChild('couriers', array('route' => 'couriers', 'label' => 'menu.couriers'));
        $menu['home']['couriers']->addChild('courier_show', array(
                'routeParameters' => array('slug' => $request->get('slug')),
                'route' => 'courier_show',
                'label' => 'menu.showCourier'
                ))->setDisplay(false);
        $menu['home']->setChildrenAttribute('class', 'nav navbar-nav navbar-right');

        // if user is logged in
        if ($this->container->get('security.context')->isGranted(array('ROLE_ADMIN', 'ROLE_USER'))) {

            $menu['home']->addChild('myaccount', array('label' => 'menu.myaccount'))
                ->setAttribute('dropdown', true);
            $menu['home']['myaccount']->addChild('profile', array('route' => 'fos_user_profile_edit', 'label' => 'menu.profileEdit'));
            $menu['home']['myaccount']->addChild('offer', array('route' => 'offer', 'label' => 'menu.offerEdit'));
            $menu['home']['myaccount']->addChild('price', array('route' => 'price', 'label' => 'menu.myPriceList'));
            $menu['home']['myaccount']['price']->addChild('new_price', array('route' => 'new_price', 'label' => 'menu.addPriceList'));
            $menu['home']['myaccount']['price']->addChild('add_price', array('route' => 'add_price', 'label' => 'menu.addPriceList'));

            
            $menu['home']['myaccount']['price']->addChild('edit_price', array(
                'routeParameters' => array('id' => $id),
                'route' => 'edit_price',
                'label' => 'menu.editPriceList'
                ))->setDisplay(false);

            // $menu['home']['myaccount']->addChild('membership', array('route' => 'membership', 'label' => 'menu.membership'));
            $menu['home']->addChild('logout', array('route' => 'fos_user_security_logout', 'label' => 'menu.logout'))
                ->setLinkAttribute('class', 'signup');


        }
        // otherwise
        else {

            $menu['home']->addChild('signup', array('label' => 'menu.login'))
                ->setUri('#')
                ->setLinkAttribute('class', 'signup')
                ->setLinkAttribute('data-toggle', 'modal')
                ->setLinkAttribute('data-target', '#modal-signin');

        }

        return $menu;
    }

    public function footerMenu(FactoryInterface $factory, array $options) {

        $menu = $factory->createItem('root');
        $menu->addChild('home', array('route' => 'homepage', 'label' => 'menu.home'));
        $menu['home']->addChild('tand', array('route' => 'tandc', 'label' => 'menu.termsandcondition'));
        $menu['home']->addChild('sitemap', array('route' => 'sitemap', 'label' => 'menu.sitemap'));
        $menu['home']->addChild('cookieinformations', array('route' => 'cookieinformations', 'label' => 'menu.cookieinformations'));
        $menu['home']->addChild('contact', array('route' => 'mremi_contact_form', 'label' => 'menu.contact'));

        return $menu;
    }

}