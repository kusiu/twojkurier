<?php

namespace AppBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Doctrine\ORM\EntityManager;

class UserService
{

    const COURIER_NO = 5;

    private $container;

    private $tokenStorage;

    private $entityManager;

    public function __construct(ContainerInterface $container, TokenStorage $tokenStorage, EntityManager $entityManager) {
        $this->container = $container;
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
    }

    /**
     * Returns list of latest couriers
     */
    public function getLatestCouriers()
    {
        return $this->entityManager
            ->getRepository('AppBundle:User')
            ->findLatest(UserService::COURIER_NO);
    }

    /**
     * Check if user data provided is completed
     *
     * @param boolean $message Returns message.
     * @return boolean
     */
    public function isCompleted($message = false)
    {

        $info = [];
        $user = $this->tokenStorage->getToken()->getUser();

        // If anonymous
        if (!is_object($user)) {
            return;
        }

        // Check if user profile is complete
        if (!$user->getPolishPhoneNumber() || !$user->getBritishPhoneNumber()
            || !$user->getContactEmail() || !$user->getCompany()
        ) {
            $info[] = '<a href="'. $this->container->get('router')->generate('fos_user_profile_edit') .'">Kliknij <b>tu</b> aby uzupełnić dane kontaktowe</a>';
        }

        // Check collection days
        foreach ($user->getCollections() as $collection) {
            if (count($collection->getDays()) == 0) {
                $info[] = '<a href="'. $this->container->get('router')->generate('offer') .'">Kliknij <b>tu</b> aby zaznaczyć dni odbioru paczki</a>';
                break;
            }
        }

        // Check offer
        if ($user->getOffers()->count() == 0) {
            $info[] = '<a href="'. $this->container->get('router')->generate('price') .'">Kliknij <b>tu</b> aby uzupełnić swoją listę cenową</a>';
        }

        if ($message == false) {
            return count($info) == 0;
        }

        $message = '';
        foreach ($info as $item) {
            $message .= '<li>'. $item .'</li>';
        }


        if ($info) {
            $this->container->get('request')->getSession()->getFlashBag()->set(
                'info user-notification',
                '
                        <b>Twoja oferta nie jest jeszcze widoczna. Prosimy Cię o uzupełnienie informacji:</b><br />
                        <ul>'.  $message .'</ul>
                    '
            );

        }

        if ($message == false && empty($info)) {
            return true;
        }
        return false;
    }

}