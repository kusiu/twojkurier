$(document).ready(function(){

    /* nice scroll */
    $( 'html' ).niceScroll({
        cursorcolor: '#434a54',
        cursorwidth: '10px',
        cursorborder: '1px solid #434a54',
        cursoropacitymax: 0.9,                
        scrollspeed: 110,
        autohidemode: false,
        horizrailenabled: false,
        cursorborderradius: 4,
        zindex: 1060
    });

    /* scrolltop */
    $('.scroltop').on('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });

    /* modal */
    $('.modal').on('shown.bs.modal', function () {
        var curModal = this;
        $('.modal').each(function(){
            if(this !== curModal){
                $(this).modal('hide');
            }
        });
    });


    /* tooltip */
    $('[rel="tooltip"]').tooltip();


    // swap options values
    
    var previousValue;

    $(".country-from").on('focus', function () {
        previousValue = this.value;
    }).change(function() {
        $('.country-to').val(previousValue);
        previousValue = this.value;
    });
    
    $(".country-to").on('focus', function () {
        previousValue = this.value;
    }).change(function() {
        $('.country-from').val(previousValue);
        previousValue = this.value;
    });

    });
