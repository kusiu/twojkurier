<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Request;

/**
 * Default controller.
 *
 * @Route("/")
 */
class DefaultController extends Controller
{
    /**
     *
     * @Route("/", name="homepage", options={"sitemap" = true})
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        
        $form = $this->createFormBuilder()
            ->add('countryFrom', 'entity', array(
                'class'    => 'AppBundle:Country',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.name', 'ASC');
                },
                'label'     => 'filter.from',
                'attr'      => array (
                    'class' => 'country-from',
                )
            ))
            ->add('countryTo', 'entity', array(
                'class'     => 'AppBundle:Country',
                'label'     => 'filter.to',
                'attr'      => array (
                    'class' => 'country-to',
                )
            ))
            ->add('weight', 'integer', array(
                'label'     => 'filter.weight',
                'attr'      => array (
                    'class' => 'weight',
                )
            ))
            ->setAction($this->generateUrl('couriers'))
            ->setMethod('GET')
        ->getForm();
                
        return array(
            'searchForm' => $form->createView(),
            'latestCouriers' => $this->get('app.service.user')->getLatestCouriers()
        );
    
    }
    
    /**
     *
     * @Route("/couriers", name="couriers", options={"sitemap" = true})
     * @Method("GET")
     * @Template()
     */
    public function couriersAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('countryFrom', 'entity', array(
                'class'    => 'AppBundle:Country',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.name', 'ASC');
                },
                'label'     => 'filter.from',
                'attr'      => array (
                    'class' => 'country-from',
                )
            ))
            ->add('countryTo', 'entity', array(
                'class'     => 'AppBundle:Country',
                'label'     => 'filter.to',
                'attr'      => array (
                    'class' => 'country-to',
                )
            ))
            ->add('weight', 'integer', array(
                'required'  => true,
                'label'     => 'filter.weight',
                'attr'      => array (
                    'class' => 'weight',
                    'input_group' => array('append' => 'kg')
                ),
                'data' => 30
            ))
            ->add('days', 'entity', array(
                'class'         => 'AppBundle:Day',
                'label'         => 'filter.collection',
                'expanded'      => true,
                'multiple'      => false,
                'placeholder'   => 'filter.default',
                'empty_data'  => null,
                'required'      => false
            ))       
            ->setMethod('GET')
        ->getForm();
                
        $em = $this->getDoctrine()->getManager();

        $search = false;
                
        if ($request->query->has($form->getName())) {

            $form->handleRequest($request);
            $data = $request->query->get('form');

            $result = $em->getRepository('AppBundle:Offer')
                    ->findAllActiveBySearch($data);
            
            $search = true;

            $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $result,
                $this->get('request')->query->get('page', 1),
                10
            );
            
        } else  {
            
            $result = $em->getRepository('AppBundle:User')
                    ->findAllActive();

            $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $result,
                $this->get('request')->query->get('page', 1),
                10,
                array('defaultSortFieldName' => 'u.createdAt', 'defaultSortDirection' => 'desc')
            );
            
        }
                
        return array(
            'searchForm'    => $form->createView(),
            'pagination'    => $pagination,
            'search'        => $search
        );
    }
    
    /**
     *
     * @Route("couriers/{slug}", name="courier_show", defaults={"slug" = false}, options={"sitemap" = true})
     * @Method("GET")
     * @Template()
     */
    public function courierShowAction($slug)
    {
        
        $em = $this->getDoctrine()->getManager();
        
        $result = $em->getRepository('AppBundle:User')
            ->findBySlug($slug);
        
        return array(
            'courier'   => $result
        );
        
    }

    /**
     *
     * @Route("/termsandconditions", name="tandc", options={"sitemap" = true})
     * @Method("GET")
     * @Template()
     */
    public function tandcAction()
    {
        return [];
    }

    /**
     *
     * @Route("/sitemap", name="sitemap", options={"sitemap" = true})
     * @Method("GET")
     * @Template()
     */
    public function sitemapAction()
    {
        return [];
    }

    /**
     *
     * @Route("/cookieinformations", name="cookieinformations", options={"sitemap" = true})
     * @Method("GET")
     * @Template()
     */
    public function cookieinformationsAction()
    {
        return [];
    }
    
}
