<?php

namespace AppBundle\Controller;

use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Offer;
use AppBundle\Form\OfferType;
use AppBundle\Form\CollectionFormType;

/**
 * Default controller.
 *
 * @Route("/myaccount")
 */
class MyAccountController extends Controller
{

    /**
     *
     * @Route("/membership", name="membership")
     * @Method("GET")
     * @Template()
     */
    public function membershipAction()
    {

        $lastMembership = $this->getUser()->getMemberships()->first();
        $lastMembership->getDateTo();
        $currentDate = new DateTime(date('Y-m-d'));
        $endDate = $lastMembership->getDateTo();
        $interval = $currentDate->diff($endDate);

        return array (
            'daysLeft' => $interval->format('%a'),
            'memberships' => $this->getUser()->getMemberships()
        );
    }

    /**
     * Creates a form to create a Offer entity.
     *
     * @param Offer $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Offer $entity)
    {
        
        $form = $this->createForm(new OfferType(), $entity, array(
            'action' => $this->generateUrl('add_price'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Dalej'));

        return $form;
    }
    
    /**
     *
     * @Route("/offer", name="offer")
     * @Method("GET")
     * @Template()
     */
    public function offerAction()
    {
        
        // Collection form
        $form = $this->createForm(new CollectionFormType(), $this->getUser(), array(
            'action' => $this->generateUrl('collection_update'),
            'method' => 'PUT',
        ));

        return array(
            'form'  => $form->createView()
        );

    }

    /**
     *
     * @Route("/price", name="price")
     * @Method("GET")
     * @Template()
     */
    public function priceAction()
    {

        $em = $this->getDoctrine()->getManager();

        $prices = $em->getRepository('AppBundle:Offer')
            ->findByUser($this->getUser());

        return array(
            'prices' => $prices,
        );

    }
    
    /**
     * Edits an existing Offer entity.
     *
     * @Route("/offer", name="collection_update")
     * @Method("PUT")
     * @Template("AppBundle:MyAccount:offer.html.twig")
     */
    public function collectionUpdateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $form = $this->createForm(new CollectionFormType(), $user, array(
            'action' => $this->generateUrl('collection_update'),
            'method' => 'PUT',
        ));
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            
            $em->flush();
            
            $this->get('session')->getFlashBag()->set(
                'success', 
                "Dane zostały zaktualizowane."
            );
            
        }
        
        $prices = $em->getRepository('AppBundle:Offer')
                ->findByUser($this->getUser());
        
        
        return array(
            'prices' => $prices,
            'form'  => $form->createView()
        );
    }
    

    /**
     *
     * @Route("/price/new", name="new_price")
     * @Method("GET")
     * @Template()
     */
    public function newOfferAction()
    {

        $entity = new Offer();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );

    }
    
    /**
     *
     * @Route("/price/add", name="add_price")
     * @Method("POST")
     * @Template("AppBundle:MyAccount:newOffer.html.twig")
     */
    public function addOfferAction(Request $request)
    {
        $entity = new Offer();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        
        // Check weight from
        $this->checkWeight(
                $form->get('weightFrom')->getData(),
                $form->get('countryFrom')->getData(),
                $form->get('countryTo')->getData()
            );
        
        // Check weight to
        $this->checkWeight(
                $form->get('weightTo')->getData(),
                $form->get('countryFrom')->getData(),
                $form->get('countryTo')->getData()
            );
        
        if ($form->isValid() && !$this->get('session')->getFlashBag()->has('danger')) {
            $em = $this->getDoctrine()->getManager();
            $entity->setUser($this->getUser());
            
            $em->persist($entity);
            $em->flush();
            
            $this->get('session')->getFlashBag()->set(
                'success', 
                'Oferta została dodana'
            );

            // return $this->redirectToRoute('price') . '#my-price';
            return $this->redirect($this->generateUrl('price') . '#my-price');
            
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
        
    }
    
    public function checkWeight($weight, $countryFrom, $countryTo) {
        
        $em = $this->getDoctrine()->getManager();
        
        $result = $em->getRepository('AppBundle:Offer')
            ->findOneByWeight($weight, $countryFrom, $countryTo, $this->getUser());
        
        if ($result) {
            
            $this->get('session')->getFlashBag()->set(
                'danger', 
                "Oferta z podaną wagą już istnieje. ({$result[0]->getWeightFrom()} - {$result[0]->getWeightTo()} kg)"
            );
            return;
        }
        
    }

    /**
     * Displays a form to edit an existing Day entity.
     *
     * @Route("/price/{id}/edit", name="edit_price")
     * @Method("GET")
     * @Template("AppBundle:MyAccount:newOffer.html.twig")
     */
    public function editOfferAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Offer')->find($id);
        
        $this->denyAccessUnlessGranted('edit', $entity, 'Unauthorized access!');

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Offer entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'form'        => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Offer entity.
     *
     * @param Offer $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Offer $entity)
    {
        $form = $this->createForm(new OfferType(), $entity, array(
            'action' => $this->generateUrl('price_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Offer entity.
     *
     * @Route("/price/{id}", name="price_update")
     * @Method("PUT")
     * @Template("AppBundle:MyAccount:newOffer.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Offer')->find($id);
        
        $this->denyAccessUnlessGranted('edit', $entity, 'Unauthorized access!');

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Offer entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            
            $em->flush();
            
            $this->get('session')->getFlashBag()->set(
                'success', 
                "Oferta została zmieniona."
            );

            return $this->redirect($this->generateUrl('edit_price', array('id' => $id)));
            
        }

        return array(
            'entity'      => $entity,
            'form'        => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Offer entity.
     *
     * @Route("/price/{id}", name="delete_price")
     * @Method("GET")
     */
    public function deleteAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Offer')->find($id);

        $this->denyAccessUnlessGranted('edit', $entity, 'Unauthorized access!');

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Offer entity.');
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->set(
            'success', 
            'Oferta została usunięta'
        );

        return $this->redirect($this->generateUrl('price'));
    }

    /**
     * Creates a form to delete a Offer entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('delete_price', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Usuń'))
            ->getForm()
            ;
    }
    
}
