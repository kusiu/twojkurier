<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OrderBy;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Offer
 *
 * @ORM\Table(name="offer")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OfferRepository")
 */
class Offer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Country")
     * @JoinTable(name="country_from")
     **/
    private $countryFrom;

    /**
     * @ManyToOne(targetEntity="Country")
     * @JoinTable(name="country_to")
     **/
    private $countryTo;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="offers")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     **/
    private $user;

    /**
     * @var integer
     *
     * @ORM\Column(name="weight_from", type="integer")
     * @Assert\NotBlank(message="Proszę wprowadzić wagę paczki")
     * @Assert\GreaterThan(value=0, message="Proszę wprowadzić wagę paczki")
     * @Assert\Expression(
     *     "this.getWeightFrom() <= this.getWeightTo()",
     *     message="Wartośc pola 'waga od' nie może być większe od pola 'waga do'"
     * )
     */
    private $weightFrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="weight_to", type="integer")
     * @Assert\NotBlank(message="Proszę wprowadzić wagę paczki")
     * @Assert\Expression(
     *     "this.getWeightTo() >= this.getWeightFrom()",
     *     message="Wartośc pola 'waga od' nie może być mniejsze od pola 'waga do'"
     * )
     */
    private $weightTo;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal")
     * @Assert\NotBlank(message="Proszę wprowadzić cenę usługi")
     * @Assert\GreaterThan(value=0, message="Proszę wprowadzić cenę usługi")
     */
    private $price;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \stdClass $user
     *
     * @return Offer
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \stdClass
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set weightFrom
     *
     * @param integer $weightFrom
     *
     * @return Offer
     */
    public function setWeightFrom($weightFrom)
    {
        $this->weightFrom = $weightFrom;

        return $this;
    }

    /**
     * Get weightFrom
     *
     * @return integer
     */
    public function getWeightFrom()
    {
        return $this->weightFrom;
    }

    /**
     * Set weightTo
     *
     * @param integer $weightTo
     *
     * @return Offer
     */
    public function setWeightTo($weightTo)
    {
        $this->weightTo = $weightTo;

        return $this;
    }

    /**
     * Get weightTo
     *
     * @return integer
     */
    public function getWeightTo()
    {
        return $this->weightTo;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Offer
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }
    
    /**
     * Get calculated price
     *
     * @return string
     */
    public function getCalculatedPrice()
    {
        if ($this->getCountryFrom()->getId() == 1) {
            return 'PLN ' . $this->getPrice();
        }
        return '&pound; ' . $this->getPrice();
    }

    /**
     * Set countryFrom
     *
     * @param \AppBundle\Entity\Country $countryFrom
     *
     * @return Offer
     */
    public function setCountryFrom(\AppBundle\Entity\Country $countryFrom = null)
    {
        $this->countryFrom = $countryFrom;

        return $this;
    }

    /**
     * Get countryFrom
     *
     * @return \AppBundle\Entity\Country
     */
    public function getCountryFrom()
    {
        return $this->countryFrom;
    }

    /**
     * Set countryTo
     *
     * @param \AppBundle\Entity\Country $countryTo
     *
     * @return Offer
     */
    public function setCountryTo(\AppBundle\Entity\Country $countryTo = null)
    {
        $this->countryTo = $countryTo;

        return $this;
    }

    /**
     * Get countryTo
     *
     * @return \AppBundle\Entity\Country
     */
    public function getCountryTo()
    {
        return $this->countryTo;
    }

}
