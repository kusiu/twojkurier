<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;
use \DateTime;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinTable;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @Vich\Uploadable
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Proszę wpisać imię.")
     * @Assert\Length(
     *     min=2,
     *     max="20",
     *     minMessage="Imię jest za krótkie.",
     *     maxMessage="Imię jest za długie."
     * )
     */
    protected $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Proszę wpisać nazwisko.")
     * @Assert\Length(
     *     min=2,
     *     max="20",
     *     minMessage="Nazwisko jest za krótkie.",
     *     maxMessage="Nazwisko jest za długie."
     * )
     */
    protected $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Proszę wpisać nazwę firmy.")
     * @Assert\Length(
     *     min=2,
     *     max="20",
     *     minMessage="Nazwa firmy jest za krótka.",
     *     maxMessage="Nazwa firmy jest za długa."
     * )
     */
    protected $company;
    
    /**
     * @OneToMany(targetEntity="Membership", mappedBy="user")
     * @ORM\OrderBy({"dateTo" = "DESC"})
     **/
    private $memberships;
    
    /**
     * @OneToMany(targetEntity="Offer", mappedBy="user")
     **/
    private $offers;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     *
     */
    private $polishPhoneNumber;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $britishPhoneNumber;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     *
     * @Assert\Email(
     *     message = "'{{ value }}' nie jest poprawnym emailem."
     * )
     */
    private $contactEmail;
    
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     *
     * @Assert\Url(
     *     checkDNS = true,
     *     message = "'{{ value }}' nie jest poprawnym adresem url."
     * )
     */
    private $website;
    
    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     *
     */
    private $address;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="logo_image", fileNameProperty="logoName")
     * @Assert\File(
     *     maxSize="2M",
     *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
     * )
     * @var File $logo
     */
    private $logo;

    /**
     * @ORM\Column(type="string", length=255, name="logo_name", nullable=true)
     *
     * @var string $logoName
     */
    private $logoName;

    /**
     * @ORM\ManyToMany(targetEntity="Collection", cascade={"persist"})
     */
    private $collections;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime $updatedAt
     */
    private $updatedAt;

    /**
     * @Gedmo\Slug(fields={"company", "id"}, updatable=true, separator="-")
     * @ORM\Column(name="slug", type="string", length=100, nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $complete = false;

    /**
     * @ManyToOne(targetEntity="Service")
     **/
    private $services;

    public function __construct()
    {
        parent::__construct();
        parent::setUsername('empty');
        $this->memberships = new ArrayCollection();
        $this->collections = new ArrayCollection();
        $this->offers = new ArrayCollection();
        $this->createdAt = new \DateTime('now');

    }

    public function getCollections()
    {
        return $this->collections;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    public function setEmail($email)
    {

        $email = is_null($email) ? '' : $email;
        parent::setEmail($email);
        $this->setUsername($email);

    }

    /**
     * Add membership
     *
     * @param \AppBundle\Entity\Membership $membership
     *
     * @return User
     */
    public function addMembership(\AppBundle\Entity\Membership $membership)
    {
        $this->memberships[] = $membership;

        return $this;
    }

    /**
     * Remove membership
     *
     * @param \AppBundle\Entity\Membership $membership
     */
    public function removeMembership(\AppBundle\Entity\Membership $membership)
    {
        $this->memberships->removeElement($membership);
    }

    /**
     * Get memberships
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMemberships()
    {
        return $this->memberships;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     *
     * @return User
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set company
     *
     * @param string $company
     *
     * @return User
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Returns true if user has a valid membership
     *
     * @return boolean
     */
    public function hasValidMembership()
    {

        $lastMembership = $this->getMemberships()->first();

        $validTo = $lastMembership->getDateTo();

        if (new DateTime() < $validTo) {
            return true;
        }

        return false;

    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setLogo(File $image = null)
    {
        $this->logo = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param string $logoName
     */
    public function setLogoName($logoName)
    {
        $this->logoName = $logoName;
    }

    /**
     * @return string
     */
    public function getLogoName()
    {
        return $this->logoName;
    }


    /**
     * Set polishPhoneNumber
     *
     * @param string $polishPhoneNumber
     *
     * @return User
     */
    public function setPolishPhoneNumber($polishPhoneNumber)
    {
        $this->polishPhoneNumber = $polishPhoneNumber;

        return $this;
    }

    /**
     * Get polishPhoneNumber
     *
     * @return string
     */
    public function getPolishPhoneNumber()
    {
        return $this->polishPhoneNumber;
    }

    /**
     * Set britishPhoneNumber
     *
     * @param string $britishPhoneNumber
     *
     * @return User
     */
    public function setBritishPhoneNumber($britishPhoneNumber)
    {
        $this->britishPhoneNumber = $britishPhoneNumber;

        return $this;
    }

    /**
     * Get britishPhoneNumber
     *
     * @return string
     */
    public function getBritishPhoneNumber()
    {
        return $this->britishPhoneNumber;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set contactEmail
     *
     * @param string $contactEmail
     *
     * @return User
     */
    public function setContactEmail($contactEmail)
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    /**
     * Get contactEmail
     *
     * @return string
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    public function addCollection(Collection $collection)
    {

        if (!$this->collections->contains($collection)) {
            $this->collections->add($collection);
        }
    }


    /**
     * Remove collection
     *
     * @param \AppBundle\Entity\Collection $collection
     */
    public function removeCollection(\AppBundle\Entity\Collection $collection)
    {
        $this->collections->removeElement($collection);
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return User
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Add offer
     *
     * @param \AppBundle\Entity\Offer $offer
     *
     * @return User
     */
    public function addOffer(\AppBundle\Entity\Offer $offer)
    {
        $this->offers[] = $offer;

        return $this;
    }

    /**
     * Remove offer
     *
     * @param \AppBundle\Entity\Offer $offer
     */
    public function removeOffer(\AppBundle\Entity\Offer $offer)
    {
        $this->offers->removeElement($offer);
    }

    /**
     * Get offers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return User
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set complete
     *
     * @param boolean $complete
     *
     * @return Offer
     */
    public function setComplete($complete)
    {
        $this->complete = $complete;

        return $this;
    }

    /**
     * Get complete
     *
     * @return boolean
     */
    public function getComplete()
    {
        return $this->complete;
    }

    /**
     * Set services
     *
     * @param \AppBundle\Entity\Service $services
     *
     * @return User
     */
    public function setServices(\AppBundle\Entity\Service $services = null)
    {
        $this->services = $services;

        return $this;
    }

    /**
     * Get services
     *
     * @return \AppBundle\Entity\Service
     */
    public function getServices()
    {
        return $this->services;
    }
}
