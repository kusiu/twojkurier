<?php

namespace AppBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class AppExtension extends \Twig_Extension
{

    private $container;

    private $tokenStorage;

    public function __construct(ContainerInterface $container, TokenStorage $tokenStorage) {
        $this->container = $container;
        $this->tokenStorage = $tokenStorage;
    }

    public function getFunctions()
    {
        return array(
            'notification' => new \Twig_Function_Method($this, 'userNotification')
        );
    }

    public function userNotification()
    {
        $this->container->get('app.service.user')->isCompleted(true);
    }

    public function getName()
    {
        return 'app_extension';
    }
}