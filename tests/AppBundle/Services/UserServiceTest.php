<?php

namespace Tests\AppBundle\Services;

use AppBundle\Services\UserService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use AppBundle\Entity\User;

class UserServiceTest extends \PHPUnit_Framework_TestCase
{

    private $entityManager;

    private $tokenStorage;

    private $container;

    private $service;

    public function setUp()
    {
        $this->entityManager  = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->tokenStorage = $this->getMockBuilder()
            ->disableOriginalConstructor(TokenStorage::class)
            ->getMock();

        $this->container = $this->getMockBuilder()
            ->disableOriginalConstructor(ContainerInterface::class)
            ->getMock();

        $this->service = new UserService($this->container, $this->tokenStorage, $this->entityManager);
    }

    public function testGetLatestCouriers()
    {
        $repository = $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        var_dump($this->entityManager->getRepository());

        $this->entityManager->expects($this->once())
            ->method('getRepository')
            ->with(User::class)
            ->will($this->returnValue($repository));

    }
}